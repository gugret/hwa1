/**
 * Sorting of balls.
 *
 * @since 1.8
 */
public class ColorSort {

    enum Color {red, green, blue}

    ;

    static ColorSort.Color[] balls = null;

//   static int redCountInternal = 0;
//   static int greenCountInternal = 0;
//   static int blueCountInternal = 0;


    public static void main(String[] param) {
//      reorder(balls);// for debugging
    }

    public static void reorder(Color[] balls) {

        // Count and overwrite the array in whatever order needed
        int redCountInternal = 0;
        int greenCountInternal = 0;
        int blueCountInternal = 0;
        Color red = Color.red;
        Color green = Color.green;
        Color blue = Color.blue;

        for (int i = 0; i < balls.length; i++) { // Counts the amount of red, blue, and greens respectively
            if (balls[i] == red) {
                redCountInternal++;
            } else if (balls[i] == blue) {
                blueCountInternal++;
            } else if (balls[i] == green) {
                greenCountInternal++;
            }
        }

//        for (int i = 0; i < balls.length; i++) {
//            if (i < redCountInternal) { // If at less than the total count of reds, replace color with red
//                balls[i] = red;
//            }
//            // If at (or equal to) more than the count of reds but less than the count of reds and greens, replace with green
//            if (i >= redCountInternal && i < redCountInternal + greenCountInternal) {
//                balls[i] = green;
//            }
//            if (i >= redCountInternal + greenCountInternal) { // If at (or equal to) more than the count of reds and greens, replace with blue
//                balls[i] = blue;
//            }
//        }
        for (int i = 0; i < redCountInternal; i++){
           balls[i] = red;
        }
        for (int i = redCountInternal; i < redCountInternal + greenCountInternal; i++){
           balls[i] = green;
        }
        for (int i = redCountInternal+greenCountInternal; i < balls.length; i++){
           balls[i] = blue;
        }
    }
}

